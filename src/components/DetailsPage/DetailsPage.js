import React from "react";
import "./styleDetailsPage.scss";

const DetailsPage = (props) => {
  const { info, pageSelection } = props;
  let game = info;
  let gameParse = JSON.parse(game);

  return (
    <div className={pageSelection}>
      <h1 className="title-details">{gameParse?.title}</h1>
      <div className="details-products">
        <div className="detail-product">
          <a href="#h">
            <div className="details-img">
              <img src={gameParse?.imgUrl} alt={gameParse?.id}></img>
            </div>
          </a>
          <div className="details-footer">
            <h1> {gameParse?.title} </h1>
            <p className="detail-body"> {gameParse?.body} </p>
            <p className="detail-platform">DATE: {gameParse?.date}</p>
            <p className="detail-platform">CATEGORY: {gameParse?.category}</p>
            <p className="detail-platform">PLATFORMS: {gameParse?.platform}</p>
          </div>
          <div className="detail-btn">
            <a className="detail-btn" href={gameParse?.gameUrl}>
              <button className="btn-det">OFFICIAL SITE</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailsPage;
