import React, { useState, useEffect } from "react";
import "./styleFooter.scss";

const DataLengt = () => {
  const [dataSize, setDataSize] = useState([]);

  useEffect(() => {
    const ArraySize = async () => {
      const dataBase = await ObtainDataSize();
      setDataSize(dataBase);
    };
    ArraySize();
  }, []);

  const ObtainDataSize = async () => {
    const response = await fetch(
      "https://database-homework4.herokuapp.com/games"
    );
    const data = await response.json();
    let number = data.length;
    return number;
  };

  return dataSize;
};

const Footer = (props) => {
  const { numberPage, nextPage, prevPage, setCurrentPage, pageSelection, setReloadStatus, reloadStatus } = props;


  let SizeData = DataLengt();
  let maxNumberPage = (SizeData / 24).toFixed(0);
  let arrayPages = [];

  for (let i = 0; i < maxNumberPage; i++) {
    let a = {
      id: i + 1,
      page: i + 1,
    };

    arrayPages.push(a);
  }

  let currentPage = numberPage;

  const [pagePosition, setPagePosition] = useState(currentPage);
  const [clicked, setClicked] = useState("false");

  const HandleChange = (page) => {
    setCurrentPage(page)
    setReloadStatus(!reloadStatus);
    setPagePosition(page)

  };

  const FunctionsNextPage = () => {
    let newPage = currentPage + 1;
    setPagePosition(newPage);
    setClicked(true);
    nextPage();
  };
  const FunctionsPrevPage = () => {
    let newPage = currentPage - 1;
    setPagePosition(newPage);
    setClicked(true);
    prevPage();
  };

  return (
    <div className={pageSelection}>
      <div className="pagination">
        <button className="BtnNextPrev" onClick={FunctionsPrevPage}>
          PREVIOUS
        </button>

        {arrayPages.length
          ? arrayPages?.map((page) => {
            return (
              <button
                key={page.id}
                clicked={clicked.toString()}
                onClick={(() => HandleChange(page.page))}
                className={`pageNumberList ${pagePosition === page.id && clicked ? " active" : ""
                  }`}
              >
                {page.page}
              </button>
            );
          })
          : null}

        <button className="BtnNextPrev" onClick={FunctionsNextPage}>
          NEXT
        </button>
      </div>
    </div>
  );
};

export default Footer;
