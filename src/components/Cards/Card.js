import React, { useState } from "react";

import "./styleCards.scss";

export default function Card(props) {
  const { id, title, imgUrl, gameUrl, body, platform, date, category, setState } = props
  const [clicked, setClicked] = useState(false);
  // const [detailsState, setDetailsState] = useState("")

  const handleClick = () => {
    setClicked(!clicked);
  };

  const FuncLocal = () => {
    let infoGame = {
      id: id,
      title: title,
      imgUrl: imgUrl,
      gameUrl: gameUrl,
      body: body,
      platform: platform,
      date: date,
      category: category,
    };
    let stateDetails = true;
    localStorage.setItem("game", JSON.stringify(infoGame));
    localStorage.setItem("stateDetails", stateDetails);
  };

  const functionCombine = (e) => {
    e.preventDefault();
    FuncLocal();
    setState("Details");
  };

  return (
    <div className="product">
      <a href={gameUrl}>
        <div className="product_img">
          <img src={imgUrl} alt=""></img>
        </div>
      </a>
      <div className="product_footer">
        <h1> {title} </h1>
        <p className="body"> {body} </p>
        <p className="platform">{date}</p>
        <p className="platform">{category}</p>
      </div>
      <div className="button">
        <button
          className="btn"
          clicked={clicked.toString()}
          onClick={handleClick}
        >
          {platform}
        </button>

        <div>
          <a href="" className="btn" onClick={functionCombine}>
            {" "}
            View{" "}
          </a>
        </div>
      </div>
    </div>
  );
}
