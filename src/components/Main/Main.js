import React from "react";
import "./Main.scss";
import DataFetchNext from "../products/DataFetchNext";
import DataFetchPrev from "../products/DataFetchPrev";

const Main = (props) => {
  const { reload, numberPage, setState, pageSelection, setApiState } = props;
  let status = reload;
  return (
    <div className={pageSelection}>
      {!status ? (
        <DataFetchNext
          page={numberPage}
          setState={setState}
          setApiState={setApiState}
        />
      ) : (
        <DataFetchPrev
          page={numberPage}
          setState={setState}
          setApiState={setApiState}
        />
      )}
    </div>
  );
};

export default Main;
