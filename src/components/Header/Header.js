import React, { useState } from "react";
import "./styleHeader.scss";
import BurguerButton from "./BurguerButton";

const Header = (props) => {
  const { setState } = props;
  const [clicked, setClicked] = useState(false);
  const handleClick = () => {
    //Cuando esta true lo pasa a false y viceversa
    setClicked(!clicked);
    setState("Home");
  };
  return (
    <header>
      <div className="navContainer">
        <h2>
          VideoGames <span> Stores </span>
        </h2>
        <div className={`links ${clicked ? "active" : ""}`}>
          <a onClick={handleClick} href="">
            Home
          </a>
          <a onClick={handleClick} href="https://www.freetogame.com/">
            Oficial Site
          </a>

        </div>

        <div className="burguer">
          {/* Estas son las props */}
          <BurguerButton
            clicked={clicked}
            handleClick={handleClick}
          ></BurguerButton>
        </div>

        <div className={`BgDiv initial ${clicked ? " active" : ""}`}></div>
      </div>
    </header>
  );
};

export default Header;
