import React from "react";
import "./LoaderStyle.scss";

const Loader = (props) => {
  const { apiState } = props;
  return (
    <div className={apiState === "Charging" ? "Charging" : "Arrived"}>
      {" "}
      LOADING...{" "}
    </div>
  );
};

export default Loader;
