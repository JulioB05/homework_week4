import React, { useState, useEffect } from "react";
import Card from "../Cards/Card";

const DataFetchPrev = (props) => {
  const { page, setState, setApiState } = props;
  const [games, setGames] = useState([]);

  useEffect(() => {
    const Datos = async () => {
      const dataBase = await ObtainData();
      setGames(dataBase);
      setApiState("Arrived");
    };

    Datos();
  }, []);

  const ObtainData = async () => {
    setApiState("Charging");
    let URL = `https://database-homework4.herokuapp.com/games/?_limit=24&_page=${page}`;

    const response = await fetch(`${URL}`);
    const data = await response.json();

    const orderData = data.slice().sort((a, b) => a.id - b.id);
    return orderData;
  };

  return (
    <div>
      <div>
        <h1 className="title-products">Products</h1>
        <div className="products">
          {games.length
            ? games?.map((game) => {
              return (
                <Card
                  key={game.id}
                  id={game.id}
                  title={game.title}
                  imgUrl={game.thumbnail}
                  gameUrl={game.game_url}
                  body={game.short_description}
                  platform={game.platform}
                  date={game.release_date}
                  category={game.genre}
                  setState={setState}
                ></Card>
              );
            })
            : null}
        </div>
      </div>
    </div>
  );
};

export default DataFetchPrev;
