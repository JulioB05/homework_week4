import React, { useState, useEffect } from "react";
import DetailsPage from "./components/DetailsPage/DetailsPage";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Loader from "./components/Loader/Loader";
import "./index.scss";

function App() {
  const [currentPage, setCurrentPage] = useState(1);
  const [reloadStatus, setReloadStatus] = useState(false);
  const [state, setState] = useState("Home");
  const [apiState, setApiState] = useState("Charging");

  let dataGame = localStorage.getItem("game");
  //setCurrentPage(1);

  const nextPage = () => {
    const nextPage = currentPage + 1;

    if (currentPage < 15) {
      setCurrentPage(nextPage);

      setReloadStatus(!reloadStatus);
    }
  };

  const prevPage = () => {
    let prevPage = currentPage - 1;
    if (currentPage > 1) {
      setCurrentPage(prevPage);

      setReloadStatus(!reloadStatus);
    }
  };

  return (
    <div className="app">
      <Header setState={setState}></Header>
      <Main
        reload={reloadStatus}
        numberPage={currentPage}
        setState={setState}
        pageSelection={state === "Details" ? "invisible" : "visible"}
        setApiState={setApiState}
      ></Main>
      <DetailsPage
        info={dataGame}
        pageSelection={state === "Home" ? "invisible" : "visible"}
      ></DetailsPage>
      <Loader apiState={apiState}></Loader>
      <Footer
        numberPage={currentPage}
        nextPage={nextPage}
        prevPage={prevPage}
        setCurrentPage={setCurrentPage}
        setReloadStatus={setReloadStatus}
        reloadStatus={reloadStatus}
        pageSelection={state === "Details" ? "invisible" : "visible"}
      ></Footer>
    </div>
  );
}

export default App;
